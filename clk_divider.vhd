----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:37:44 12/11/2015 
-- Design Name: 
-- Module Name:    clk_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_divider is
	generic(	scale: positive := 8000); -- Escala para 50MHz a decimas de segundo (10Hz)
   Port ( 
				clk 	 	: in   STD_LOGIC;
				clk_out	: out  STD_LOGIC);
end clk_divider;

architecture Behavioral of clk_divider is
	signal i_clk_out:std_logic;
	signal cnt:integer range 0 to (scale/2)-1 :=0;
begin
	process (clk) is
	begin
		if (clk'event and clk='1') then
			if (cnt = (scale/2)-1) then
				i_clk_out <= not(i_clk_out);
				cnt<=0;
			else
				cnt<=cnt+1;
			end if;
		end if;
	end process;
clk_out<=i_clk_out;
end Behavioral;

