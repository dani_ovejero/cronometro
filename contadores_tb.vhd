--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:56:20 12/11/2015
-- Design Name:   
-- Module Name:   C:/Xilinx/14.7/ISE_DS/ISE/Escritorio/cronometro2/contadores_tb.vhd
-- Project Name:  cronometro2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: contadores
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY contadores_tb IS
END contadores_tb;
 
ARCHITECTURE behavior OF contadores_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT contadores
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         enable : IN  std_logic;
         load : IN  std_logic;
         s_in : IN  std_logic_vector(3 downto 0);
         count : OUT  std_logic_vector(3 downto 0);
         eoc : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '1';
   signal enable : std_logic := '1';
   signal load : std_logic := '0';
   signal s_in : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal count : std_logic_vector(3 downto 0);
   signal eoc : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: contadores PORT MAP (
          clk => clk,
          reset => reset,
          enable => enable,
          load => load,
          s_in => s_in,
          count => count,
          eoc => eoc
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= not(clk);
		clk <= '1';
		wait for clk_period/2;
		clk<=not(clk);
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      wait for clk_period*3;
			reset<='0';			
      wait for clk_period*30; 
			
      wait;
		assert false
      report "fin_sim"
      severity failure;
   end process;
END;
