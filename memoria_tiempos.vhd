library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

entity memoria_tiempos is
	generic (
		width			: positive	:=	16 -- Tama�o de palabra a almacenar
	);
   Port ( 
		clk			: in	STD_LOGIC;
		load			: in	STD_LOGIC;
		reset			: in	STD_LOGIC;
		address		: in	STD_LOGIC_VECTOR (2 downto 0);
		p_in			: in	STD_LOGIC_VECTOR (width-1 downto 0);
		p_out			: out	STD_LOGIC_VECTOR (width-1 downto 0)
	);
	
end memoria_tiempos;

architecture Behavioral of memoria_tiempos is
	
	--Se�al intermedia para datos de salida
	signal tiempo1 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo2 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo3 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo4 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo5 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo6 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo7 : STD_LOGIC_VECTOR (width-1 downto 0);
	signal tiempo8 : STD_LOGIC_VECTOR (width-1 downto 0);
	
begin
	process (clk,reset) is
	begin
		if (reset='1') then
			tiempo1 <= (others => '0');
			tiempo2 <= (others => '0');
			tiempo3 <= (others => '0');
			tiempo4 <= (others => '0');
			tiempo5 <= (others => '0');
			tiempo6 <= (others => '0');
			tiempo7 <= (others => '0');
			tiempo8 <= (others => '0');
			
		elsif (clk'event and clk='1') then
			if (load = '1') then
				case address is
					when "000" => tiempo1 <= p_in;
					when "001" => tiempo2 <= p_in;
					when "010" => tiempo3 <= p_in;
					when "011" => tiempo4 <= p_in;
					when "100" => tiempo5 <= p_in;
					when "101" => tiempo6 <= p_in;
					when "110" => tiempo7 <= p_in;
					when "111" => tiempo8 <= p_in;
					when others =>
				end case;
			end if;
		end if;
	end process;
	
	with address select
	p_out <= tiempo1 when "000",
				tiempo2 when "001",
				tiempo3 when "010",
				tiempo4 when "011",
				tiempo5 when "100",
				tiempo6 when "101",
				tiempo7 when "110",
				tiempo8 when "111",
				(others => '0') when others;
				
end Behavioral;

