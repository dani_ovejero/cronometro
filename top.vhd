----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:21:11 12/11/2015 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity top is
    Port ( 
				--ON_BOARD
				clk 	 			: in  STD_LOGIC;
            start_stop 	 	: in  STD_LOGIC;
			   reset	 			: in  STD_LOGIC;
			   leds				: out STD_LOGIC_VECTOR (3 downto 0);
				--PROTOBOARD
					--LED_DISP
					segment 		: out  STD_LOGIC_VECTOR (6 downto 0);
					anodes  		: out  STD_LOGIC_VECTOR (3 downto 0);
					--IO
					VCC			: out STD_LOGIC;
					A_encoder	: in 	STD_LOGIC;
					B_encoder	: in	STD_LOGIC;
					hold 	 		: in  STD_LOGIC
			 );
end top;

architecture Behavioral of top is

	--Modulo contador
	COMPONENT contadores
	GENERIC(
		end_of_count : unsigned(3 downto 0)
		);
	PORT(
		clk 		: IN std_logic;
		reset 	: IN std_logic;
		enable 	: IN std_logic;
		load 		: IN std_logic;
		s_in 		: IN std_logic_vector(3 downto 0);          
		count 	: OUT std_logic_vector(3 downto 0);
		eoc 		: OUT std_logic
		);
	END COMPONENT;
	
	--Detector de flancos
	COMPONENT flnk_dtct
	PORT(
		mode 		: IN  std_logic;
		input 	: IN  std_logic;
		CLK 		: IN  std_logic;
		RST 		: IN  std_logic;          
		output 	: OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT jcnt
	PORT(
		CLK : IN std_logic;
		RST : IN std_logic;          
		P_OUT : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	-- divisor de reloj
	
	COMPONENT clk_divider
	GENERIC(
		scale : positive
		);
	PORT(
		clk 		: IN  std_logic;         
		clk_out 	: OUT std_logic
		);
	END COMPONENT;
		
	--Display driver
	
	COMPONENT display_refresh
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		d1 : IN std_logic_vector(3 downto 0);
		d2 : IN std_logic_vector(3 downto 0);
		d3 : IN std_logic_vector(3 downto 0);
		d4 : IN std_logic_vector(3 downto 0);          
		display_number : OUT std_logic_vector(6 downto 0);
		display_selection : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	--Encoder_in
	
	COMPONENT encoder_in
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		A : IN std_logic;
		B : IN std_logic;          
		e_up : OUT std_logic;
		e_down : OUT std_logic
		);
	END COMPONENT;
	
	--
	COMPONENT memoria_tiempos
	PORT(
		clk : IN std_logic;
		load : IN std_logic;
		reset : IN std_logic;
		address : IN std_logic_vector(2 downto 0);
		p_in : IN std_logic_vector(15 downto 0);          
		p_out : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;
	
	-- CEREBRO
	--/////////////////////////////////////////////////////////--
	COMPONENT cerebro
	    Port ( 
			clk			: in  STD_LOGIC;
			start_stop	: in  STD_LOGIC;
         hold			: in  STD_LOGIC;
			reset			: in	STD_LOGIC;
			up				: in	STD_LOGIC;
			down			: in	STD_LOGIC;
			full			: out STD_LOGIC;
			addr			: out STD_LOGIC_VECTOR (2 downto 0);
			disp_mod		: out STD_LOGIC;
			en_out		: out	STD_LOGIC;
			reset_out	: out STD_LOGIC
		);
	END COMPONENT;

	
--SIGNALS
--////////////////////////////////////////////////////////////--
	--Flancos internos
	signal start_stop_flk, hold_flk, reset_flk : STD_LOGIC;
	signal up_flk, down_flk : STD_LOGIC;
	signal addrs : STD_LOGIC_VECTOR (2 downto 0);
	
	--Distribucion de relojes RT
	signal clk_ds : STD_LOGIC;
	signal clk_rf : STD_LOGIC;
	--Se�ales internas
		--Displays
		signal tiempo_disp		: STD_LOGIC_VECTOR(15 downto 0);
		signal dsp_mod				: STD_LOGIC;
		--eoc
		signal o_eoc_1				: STD_LOGIC;
		signal o_eoc_2				: STD_LOGIC;
		signal o_eoc_3				: STD_LOGIC;
		signal o_eoc_4				: STD_LOGIC;
		--Se�ales "Cerebro":
		signal enable_control	: STD_LOGIC;
		signal reset_control		: STD_LOGIC;
		signal tiempo_actual		: STD_LOGIC_VECTOR(15 downto 0);
		signal tiempo_cargado	: STD_LOGIC_VECTOR(15 downto 0);
		signal carga				: STD_LOGIC;
		signal load					: STD_LOGIC;
		signal full					: STD_LOGIC;
		--Se�anles memoria
		signal tiempo_mem			: STD_LOGIC_VECTOR(15 downto 0);
		
--////////////////////////////////////////////////////////////--	

begin
	
	--Detector de flancos para el start/stop
	Inst_flnk_dtct_start_stop: flnk_dtct PORT MAP(
		mode => '1',
		input => start_stop,
		output => start_stop_flk,
		CLK => clk_rf,
		RST => reset
	);
	
	--Detector de flancos para el hold
	Inst_flnk_dtct_hold: flnk_dtct PORT MAP(
		mode => '1',
		input => hold,
		output => hold_flk,
		CLK => clk_rf,
		RST => '0'
	);
	
	--Detector de flancos para el reset
	Inst_flnk_dtct_reset: flnk_dtct PORT MAP(
		mode => '1',
		input => reset,
		output => reset_flk,
		CLK => clk_rf,
		RST => '0'
	);
	
	--/////////////////////////////////////////////////////////////--
	--Cerebro
	Inst_cerebro: cerebro PORT MAP(
		clk => clk_rf,
		start_stop => start_stop_flk,
		hold => hold_flk,
		reset => reset_flk,
		up => up_flk,
		down => down_flk,
		full => full,
		addr => addrs,
		disp_mod => dsp_mod,
		en_out => enable_control,
		reset_out => reset_control
	);
	
	
	--/////////////////////////////////////////////////////////////--
	--generadores de reloj
	Inst_clk_divider_ds: clk_divider 
	GENERIC MAP (
		scale => 800000
	)
	PORT MAP(
		clk => clk,
		clk_out => clk_ds
	);
	
	Inst_clk_divider_rf: clk_divider 
	GENERIC MAP (
		scale => 24000
	)
	PORT MAP(
		clk => clk,
		clk_out => clk_rf
	);
	
	--/////////////////////////////////////////////////////////////--
	--Contadores
	Inst_cnt_1: contadores 
	GENERIC MAP (end_of_count => "1001")
	PORT MAP(
		clk => clk_ds,
		reset => reset_control,
		enable => enable_control,
		load => carga,
		s_in => tiempo_cargado(3 downto 0),
		count => tiempo_actual(3 downto 0),
		eoc => o_eoc_1
	);
	
	Inst_cnt_2: contadores 
	GENERIC MAP (end_of_count => "1001")
	PORT MAP(
		clk => o_eoc_1,
		reset => reset_control,
		enable => enable_control,
		load => carga,
		s_in => tiempo_cargado(7 downto 4),
		count => tiempo_actual(7 downto 4),
		eoc => o_eoc_2
	);
	
	Inst_cnt_3: contadores 
	GENERIC MAP (end_of_count => "0101")
	PORT MAP(
		clk => o_eoc_2,
		reset => reset_control,
		enable => enable_control,
		load => carga,
		s_in => tiempo_cargado(11 downto 8),
		count => tiempo_actual(11 downto 8),
		eoc => o_eoc_3
	);
	
	Inst_cnt_4: contadores 
	GENERIC MAP (end_of_count => "1001")
	PORT MAP(
		clk => o_eoc_3,
		reset => reset_control,
		enable => enable_control,
		load => carga,
		s_in => tiempo_cargado(15 downto 12),
		count => tiempo_actual(15 downto 12),
		eoc => leds(0)
	);
	
	--/////////////////////////////////////////////////////////////--
	--Display refresh
	Inst_display_refresh: display_refresh PORT MAP(
		clk => clk_rf,
		reset => reset,
		d1 => tiempo_disp(3 downto 0),
		d2 => tiempo_disp(7 downto 4),
		d3 => tiempo_disp(11 downto 8),
		d4 => tiempo_disp(15 downto 12),
		display_number => segment,
		display_selection => anodes
	);
	
	--/////////////////////////////////////////////////////////////--
	--Encoder_in
	
	Inst_encoder_in: encoder_in PORT MAP(
		clk => clk_rf,
		reset => start_stop_flk,
		A => A_encoder,
		B => B_encoder,
		e_up => up_flk,
		e_down => down_flk
	);
	
	VCC <= '1';
	--leds(0)<='0';
	leds(1)<=full;
	leds(2)<='1';
	leds(3)<='0';
	
	--/////////////////////////////////////////////////////////////--
	--Memoria
	Inst_memoria_tiempos: memoria_tiempos PORT MAP(
		clk => clk_rf,
		load => load,
		reset => reset_control,
		address => addrs,
		p_in => tiempo_actual,
		p_out => tiempo_mem
	);
	
	--Condensamos cuentas en 1 unica se�al
	carga <= '0';

	tiempo_cargado <= (others => '0');

	load <= enable_control and hold_flk and not(full);

	tiempo_disp <= tiempo_mem when dsp_mod = '1' else tiempo_actual;

end Behavioral;

