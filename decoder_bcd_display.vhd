----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:25:41 11/06/2015 
-- Design Name: 
-- Module Name:    decoder_bcd_display - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_bcd_display is
    Port ( 
				code : in  STD_LOGIC_VECTOR (3 downto 0);
            led : out  STD_LOGIC_VECTOR (6 downto 0)
			);
			
end decoder_bcd_display;


ARCHITECTURE dataflow OF decoder_bcd_display IS
BEGIN
	WITH code SELECT
		led <= "1111101" WHEN "1001",
			  	 "0001001" WHEN "1000",
				 "1010111" WHEN "0111",
				 "0011111" WHEN "0110",
				 "0101011" WHEN "0101",
				 "0111110" WHEN "0100",
				 "1111110" WHEN "0011",
				 "0001101" WHEN "0010",
				 "1111111" WHEN "0001",
				 "0111111" WHEN "0000",
				 "1111110" WHEN others;
END ARCHITECTURE dataflow;




