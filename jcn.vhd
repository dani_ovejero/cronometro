----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:15:12 12/11/2015 
-- Design Name: 
-- Module Name:    johnson_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity jcnt is
 Port ( 
  CLK : in  STD_LOGIC;
  RST : in  STD_LOGIC;
  P_OUT : out  STD_LOGIC_VECTOR (3 downto 0)
 );
end jcnt;

architecture Behavioral of jcnt is

 SIGNAL REG_C : std_logic_vector(3 DOWNTO 0);

 begin
 process(CLK,RST)
 begin
  if (RST = '1') then REG_C <= "1000" ;
  elsif (CLK'event and CLK = '1' and RST = '0') 
   then REG_C <= (REG_C(0) & REG_C(3 downto 1));
  end if;
 end process;
 
 P_OUT <= not(REG_C);

end architecture Behavioral;

