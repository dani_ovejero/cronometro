--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:10:17 01/22/2016
-- Design Name:   
-- Module Name:   C:/Users/USER1/Documents/cronometro3/cerebro_tb.vhd
-- Project Name:  cronometro_final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: cerebro
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY cerebro_tb IS
END cerebro_tb;
 
ARCHITECTURE behavior OF cerebro_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT cerebro
    PORT(
         clk : IN  std_logic;
         start_stop : IN  std_logic;
         hold : IN  std_logic;
         reset : IN  std_logic;
         up : IN  std_logic;
         down : IN  std_logic;
         full : OUT  std_logic;
         addr : OUT  std_logic_vector(2 downto 0);
         disp_mod : OUT  std_logic;
         en_out : OUT  std_logic;
         reset_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal start_stop : std_logic := '0';
   signal hold : std_logic := '0';
   signal reset : std_logic := '1';
   signal up : std_logic := '0';
   signal down : std_logic := '0';

 	--Outputs
   signal full : std_logic;
   signal addr : std_logic_vector(2 downto 0);
   signal disp_mod : std_logic;
   signal en_out : std_logic;
   signal reset_out : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cerebro PORT MAP (
          clk => clk,
          start_stop => start_stop,
          hold => hold,
          reset => reset,
          up => up,
          down => down,
          full => full,
          addr => addr,
          disp_mod => disp_mod,
          en_out => en_out,
          reset_out => reset_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
			reset <= '0';
		wait for 10 ns;
			start_stop <= '1';
		wait for 20 ns;
			up <= '1';
		wait for 20 ns;
			up <= '0';
		wait for 20 ns;
			down <= '1';
      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
