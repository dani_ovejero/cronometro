
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name cronometro_final -dir "C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/planAhead_run_1" -part xc6slx4cpg196-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27} }
set_property target_constrs_file "top.ucf" [current_fileset -constrset]
add_files [list {top.ucf}] -fileset [get_property constrset [current_run]]
link_design
