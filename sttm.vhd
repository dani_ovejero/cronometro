----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sttm is
    Port ( 
			clk			: in  STD_LOGIC;
			start_stop	: in  STD_LOGIC;
         hold			: in  STD_LOGIC;
			reset			: in	STD_LOGIC;
			disp_mod		: out STD_LOGIC;
			enable_out		: out	STD_LOGIC;
			reset_out	: out STD_LOGIC
		);
end sttm;

architecture Behavioral of sttm is

	type mstt is (run_mod, stop_mod, hold_mod, reset_mod);
	signal stt, nxstt		: mstt;
	signal hold_s 			: STD_LOGIC;
	signal en_hold			: STD_LOGIC;
	signal hold_mod_s		: STD_LOGIC;
	signal disp_mod_s		: STD_LOGIC;
	signal rst_out			: STD_LOGIC;
	signal en_out			: STD_LOGIC;

	COMPONENT flag
	PORT(
		activ : IN std_logic;
		reset : IN std_logic;
		clk	: IN std_logic;
		latch : OUT std_logic
		);
	END COMPONENT;

begin

	Inst_flag: flag PORT MAP(
		activ => hold_s,
		reset => rst_out,
		clk => clk,
		latch => en_hold
	);

process(clk) is
	begin
	if (rising_edge(clk)) then
		stt <= nxstt;
	end if;
end process;

process(stt, start_stop, hold, reset)
	begin
		case stt is
			when run_mod =>
				en_out <= '1';
				rst_out <= '0';
				hold_mod_s <= '0';
				if(start_stop = '1') then
					nxstt <= stop_mod;
				elsif(reset = '1') then
					nxstt <= reset_mod;
				else
					nxstt <= run_mod;
				end if;
			when stop_mod =>
				en_out <= '0';
				rst_out <= '0';
				hold_mod_s <= '0';
				if(start_stop = '1') then
					nxstt <= run_mod;
				elsif(hold = '1') then
					nxstt <= hold_mod;
				elsif(reset = '1') then
					nxstt <= reset_mod;
				else
					nxstt <= stop_mod;
				end if;
			when hold_mod =>
				en_out <= '0';
				rst_out <= '0';
				hold_mod_s <= '1';
				if(start_stop = '1') then
					nxstt <= run_mod;
				elsif(hold = '1') then
					nxstt <= stop_mod;
				elsif(reset = '1') then
					nxstt <= reset_mod;
				else
					nxstt <= hold_mod;
				end if;
			when reset_mod =>
				en_out <= '0';
				rst_out <= '1';
				hold_mod_s <= '0';
				if(start_stop = '1') then
					nxstt <= run_mod;
				else
					nxstt <= reset_mod;
				end if;
		end case;
	end process;
	hold_s <= hold and en_out;
	enable_out <= en_out;
	disp_mod_s <= en_hold and hold_mod_s;
	disp_mod <= disp_mod_s;
	reset_out <= rst_out;
end Behavioral;