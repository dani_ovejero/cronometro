--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:38:33 01/14/2016
-- Design Name:   
-- Module Name:   C:/Xilinx/14.7/ISE_DS/ISE/Escritorio/cronometro2/conversor_ps_4bit_tb.vhd
-- Project Name:  cronometro2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: conversor_ps_4bit
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY conversor_ps_4bit_tb IS
END conversor_ps_4bit_tb;
 
ARCHITECTURE behavior OF conversor_ps_4bit_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT conversor_ps_4bit
    PORT(
         input_0 : IN  std_logic;
         input_1 : IN  std_logic;
         input_2 : IN  std_logic;
         input_3 : IN  std_logic;
         serial_output : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal input_0 : std_logic := '0';
   signal input_1 : std_logic := '1';
   signal input_2 : std_logic := '1';
   signal input_3 : std_logic := '0';

 	--Outputs
   signal serial_output : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: conversor_ps_4bit PORT MAP (
          input_0 => input_0,
          input_1 => input_1,
          input_2 => input_2,
          input_3 => input_3,
          serial_output => serial_output
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 2 ns;	

      -- insert stimulus here 

      wait;
   end process;

END;
