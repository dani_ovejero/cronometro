----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity addres is
	 generic (width: positive:=4);
    Port (
			clk 			: in		STD_LOGIC;
        	reset			: in		STD_LOGIC;
			en				: in  	STD_LOGIC;
			last			: in		STD_LOGIC;
			view			: in		STD_LOGIC;
			up				: in  	STD_LOGIC;
			down			: in		STD_LOGIC;
			full			: out		STD_LOGIC;
			mem_addr		: out		STD_LOGIC_VECTOR (2 downto 0)
			);
end addres;

architecture Behavioral of addres is
	signal addr			: STD_LOGIC_VECTOR(2 downto 0);
	signal addr_efec	: STD_LOGIC_VECTOR(2 downto 0);
	signal full_s		: STD_LOGIC;
	signal top			: STD_LOGIC;
	signal zero			: STD_LOGIC;
begin
	process (clk,reset) is
	begin
		if reset='1' then
			addr <= "000";
			addr_efec <= "000";
		elsif (clk'event and clk='1') then
			if(up = '1') then
				if(en = '1') then
					if(full_s = '0') then
						addr_efec <= STD_LOGIC_VECTOR(unsigned(addr)+1);
					end if;
					addr <= STD_LOGIC_VECTOR(unsigned(addr)+1);
				else
					if (top = '0') then
						addr <= STD_LOGIC_VECTOR(unsigned(addr)+1);
					end if;
				end if;
			elsif(down = '1') then
					if (zero = '0') then
						addr <= STD_LOGIC_VECTOR(unsigned(addr)-1);
					end if;
			elsif(last = '1') then
				addr <= addr_efec;
			elsif(view = '1') then
				addr <= "000";
			end if;
		end if;
	end process;
	mem_addr <= addr;
	full_s <= '1' when unsigned(addr_efec) = 7 else '0';
	full <= full_s;
	top <= '1' when unsigned(addr) = unsigned(addr_efec) - 1 else '0' when full_s = '1' else '0';
	zero <= '1' when (unsigned(addr) = 0) else '0';
end Behavioral;