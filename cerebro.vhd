library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cerebro is
    Port ( 
			clk			: in  STD_LOGIC;
			start_stop	: in  STD_LOGIC;
         hold			: in  STD_LOGIC;
			reset			: in	STD_LOGIC;
			up				: in	STD_LOGIC;
			down			: in	STD_LOGIC;
			full			: out STD_LOGIC;
			addr			: out STD_LOGIC_VECTOR (2 downto 0);
			disp_mod		: out STD_LOGIC;
			en_out		: out	STD_LOGIC;
			reset_out	: out STD_LOGIC
		);
end cerebro;

architecture Behavioral of cerebro is

	COMPONENT addres
	PORT(
		clk 			: in		STD_LOGIC;
		reset			: in		STD_LOGIC;
		en				: in  	STD_LOGIC;
		last			: in		STD_LOGIC;
		view			: in		STD_LOGIC;
		up				: in  	STD_LOGIC;
		down			: in		STD_LOGIC;
		full			: out		STD_LOGIC;
		mem_addr		: out		STD_LOGIC_VECTOR (2 downto 0)
		);
	END COMPONENT;
	COMPONENT sttm
	PORT(
		clk : IN std_logic;
		start_stop : IN std_logic;
		hold : IN std_logic;
		reset : IN std_logic;          
		disp_mod : OUT std_logic;
		enable_out : OUT std_logic;
		reset_out : OUT std_logic
		);
	END COMPONENT;

	signal rst_out : STD_LOGIC;
	signal enable : STD_LOGIC;
	signal up_s : STD_LOGIC;
	signal down_s : STD_LOGIC;
	signal full_s : STD_LOGIC;
	signal zero : STD_LOGIC;
	signal disp_mod_s : STD_LOGIC;
	signal view_s: STD_LOGIC;

begin

	Inst_addres: addres PORT MAP(
		clk => clk,
		reset => rst_out,
		en => enable,
		last => start_stop,
		view => view_s,
		up => up_s,
		down => down_s,
		full => full_s,
		mem_addr => addr
	);
	Inst_sttm: sttm PORT MAP(
		clk => clk,
		start_stop => start_stop,
		hold => hold,
		reset => reset,
		disp_mod => disp_mod_s,
		enable_out => enable,
		reset_out => rst_out
	);
	
	full <= full_s;
	en_out <= enable;
	reset_out <= rst_out;
	disp_mod <= disp_mod_s;
	up_s <= '1' when (up = '1' and enable = '0') else '1' when (hold = '1' and enable = '1') else '0';
	down_s <= '1' when (down = '1' and enable = '0') else '0';
	view_s <= '1' when (hold = '1' and enable = '0' and disp_mod_s = '0') else '0';
end Behavioral;

