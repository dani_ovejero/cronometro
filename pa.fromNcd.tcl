
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name cronometro_final -dir "C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/planAhead_run_4" -part xc6slx4cpg196-2
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27} }
set_property target_constrs_file "top.ucf" [current_fileset -constrset]
add_files [list {top.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/top.ncd"
if {[catch {read_twx -name results_1 -file "C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/top.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"C:/Users/bot/Desktop/dani_ovejero-cronometro-510dd6ff2f27/top.twx\": $eInfo"
}
