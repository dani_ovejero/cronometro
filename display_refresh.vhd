library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity display_refresh is
    Port (
				clk 	 			  	  	: in  std_logic;
				reset 			  	  	: in  std_logic;
				d1		 			  	  	: in  STD_LOGIC_VECTOR (3 downto 0);
				d2		 			  	  	: in  STD_LOGIC_VECTOR (3 downto 0);
				d3				 		  	: in  STD_LOGIC_VECTOR (3 downto 0);
				d4				 		  	: in  STD_LOGIC_VECTOR (3 downto 0);
				display_number 	 	: out STD_LOGIC_VECTOR (6 downto 0);
				display_selection 	: out STD_LOGIC_VECTOR (3 downto 0)
			);
end display_refresh;

architecture Behavioral of display_refresh is
	signal i_display_number : std_logic_vector (3 downto 0);
	signal i_display_selection : std_logic_vector (3 downto 0);
	signal state : std_logic :='0';
	
	COMPONENT decoder_bcd_display
	PORT(
		code : IN std_logic_vector(3 downto 0);          
		led : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	COMPONENT jcnt
	PORT(
		CLK : IN std_logic;
		RST : IN std_logic;          
		P_OUT : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
begin

	--Dselect
	Inst_jcnt: jcnt PORT MAP(
		CLK => clk,
		RST => reset,
		P_OUT => i_display_selection 
	);
	
	display_selection <= i_display_selection;
	
	with i_display_selection select
	i_display_number <= d1 when "1110",
							  d2 when "1101",
							  d3 when "1011",
							  d4 when "0111",
							  "0000" when others;
	
	--Decoder BCD decimas de segundo
	Inst_decoder_bcd_display: decoder_bcd_display PORT MAP(
		code => i_display_number,
		led => display_number
	);

end Behavioral;

