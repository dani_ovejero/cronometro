--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:28:10 11/06/2015
-- Design Name:   
-- Module Name:   C:/Users/sed/Documents/Joseph Lizarme/decoder_dani_guille/decoder_tb.vhd
-- Project Name:  decoder_dani_guille
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: decoder_bcd_display
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY decoder_bcd_display_tb IS
END decoder_bcd_display_tb;
 
ARCHITECTURE behavior OF decoder_bcd_display_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT decoder_bcd_display
    PORT(
         code : IN  std_logic_vector(3 downto 0);
         led : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    
	SIGNAL code : std_logic_vector(3 DOWNTO 0);
	SIGNAL led : std_logic_vector(6 DOWNTO 0);
	
	TYPE vtest is record
		code : std_logic_vector(3 DOWNTO 0);
		led : std_logic_vector(6 DOWNTO 0);
	END RECORD;
	
	TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest;
	
	CONSTANT test: vtest_vector := (
				(code => "0000", led => "0000001"),
				(code => "0001", led => "1001111"),
				(code => "0010", led => "0010010"),
				(code => "0011", led => "0000110"),
				(code => "0100", led => "1001100"),
				(code => "0101", led => "0100100"),
				(code => "0110", led => "0100000"),
				(code => "0111", led => "0001111"),
				(code => "1000", led => "0000000"),
				(code => "1001", led => "0000100"),
				(code => "1010", led => "1111110"),
				(code => "1011", led => "1111110"),
				(code => "1100", led => "1111110"),
				(code => "1101", led => "1111110"),
				(code => "1110", led => "1111110"),
				(code => "1111", led => "1111110")
		);
 
BEGIN
 
	uut: decoder_bcd_display PORT MAP(
				code => code,
				led => led
	);

	tb: PROCESS
	BEGIN
		FOR i IN 0 TO test'HIGH LOOP
			code <= test(i).code;
			WAIT FOR 20 ns;
			ASSERT led = test(i).led
			REPORT "Salida incorrecta."
			SEVERITY FAILURE;
		END LOOP;
		
		ASSERT false
		REPORT "Simulación finalizada. Test superado."
		SEVERITY FAILURE;
	END PROCESS;

END;
