--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:17:44 01/06/2016
-- Design Name:   
-- Module Name:   C:/Xilinx/14.7/ISE_DS/ISE/Escritorio/cronometro2/display_refresh_tb.vhd
-- Project Name:  cronometro2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: display_refresh
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY display_refresh_tb IS
END display_refresh_tb;
 
ARCHITECTURE behavior OF display_refresh_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT display_refresh
    PORT(
         clk : IN  std_logic;
			reset : in std_logic;
         d2 : IN  std_logic_vector(3 downto 0);
         d4 : IN  std_logic_vector(3 downto 0);
         d1 : IN  std_logic_vector(3 downto 0);
         d3 : IN  std_logic_vector(3 downto 0);
         display_number : OUT  std_logic_vector(6 downto 0);
         display_selection : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk 			 : std_logic := '0';
	signal reset : std_logic:= '1';
   signal d2 	 : std_logic_vector(3 downto 0) := (others => '0');
   signal d4 	 : std_logic_vector(3 downto 0) := (others => '0');
   signal d1 : std_logic_vector(3 downto 0) := (others => '0');
   signal d3 : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal display_number : std_logic_vector(6 downto 0);
   signal display_selection : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: display_refresh PORT MAP (
          clk => clk,
			 reset => reset,
          d2 => d2,
          d4 => d4,
          d1 => d1,
          d3 => d3,
          display_number => display_number,
          display_selection => display_selection
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= not(clk);
		clk <= '1';
		wait for clk_period/2;
		clk<=not(clk);
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for clk_period*3;	
			reset <= '0';
			d2 <= "1001";
			d4 <= "0011";
			d1 <= "0110";
			d3 <=	"0111";
      wait for clk_period*10;

      -- insert stimulus here 

      wait;
		assert false
      report "fin_sim"
      severity failure;
   end process;

END;
