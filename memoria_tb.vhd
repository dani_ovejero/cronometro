--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:51:45 01/21/2016
-- Design Name:   
-- Module Name:   C:/Users/USER1/Documents/cronometro/memoria_tb.vhd
-- Project Name:  cronometro_final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: memoria_tiempos
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY memoria_tb IS
END memoria_tb;
 
ARCHITECTURE behavior OF memoria_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT memoria_tiempos
    PORT(
         clk : IN  std_logic;
         wr : IN  std_logic;
         s_in : IN  std_logic_vector(15 downto 0);
         s_out : OUT  std_logic_vector(15 downto 0);
         enable : IN  std_logic;
         reset : IN  std_logic;
         address : IN  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal wr : std_logic := '0';
   signal s_in : std_logic_vector(15 downto 0) := (others => '0');
   signal enable : std_logic := '0';
   signal reset : std_logic := '1';
   signal address : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal s_out : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: memoria_tiempos PORT MAP (
          clk => clk,
          wr => wr,
          s_in => s_in,
          s_out => s_out,
          enable => enable,
          reset => reset,
          address => address
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
			reset <= '0';
			enable <= '1';
			wr <= '1';
			s_in <= "1001011010001101";
			address <= "000";
		wait for 20 ns;
			s_in <= "1000100100001101";
			address <= "001";
		wait for 20 ns;
			s_in <= "0111010101001001";
			address <= "010";
		wait for 20 ns;
			address <= "011";						
			s_in <= "0000101101001001";
		wait for 20 ns;
			s_in <= "1110001010111001";
			address <= "100";
		wait for 20 ns;
			address <= "101";
			s_in <= "0011001010111001";
		wait for 20 ns;
			address <= "110";
			s_in <= "1110011110111001";
		wait for 20 ns;
			address <= "111";
			s_in <= "1110101010001001";
		wait for 20 ns;
			wr <= '0';
			address <= "010";
			s_in <= "0111010101001001";
		wait for 40 ns;
			address <= "000";
			s_in <= "0111011111001001";
		wait for 20 ns;
			wr <= '1';
			address <= "100";
			s_in <= "1000100100001101";
      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
