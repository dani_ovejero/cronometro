--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:40:34 01/21/2016
-- Design Name:   
-- Module Name:   C:/Users/USER1/Documents/cronometro/encoder_in_tb.vhd
-- Project Name:  cronometro_final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: encoder_in
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY encoder_in_tb IS
END encoder_in_tb;
 
ARCHITECTURE behavior OF encoder_in_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT encoder_in
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         A : IN  std_logic;
         B : IN  std_logic;
         e_up : OUT  std_logic;
         e_down : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal A : std_logic := '0';
   signal B : std_logic := '0';

 	--Outputs
   signal e_up : std_logic;
   signal e_down : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: encoder_in PORT MAP (
          clk => clk,
          reset => reset,
          A => A,
          B => B,
          e_up => e_up,
          e_down => e_down
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
	

      -- insert stimulus here 
			A <= '1';
		wait for 10 ns;
			B <= '1';
		wait for 20 ns;
			A <= '0';
		wait for 20 ns;
			A <= '1';
			B <= '0';
		wait for 50 ns;
			B <= '1';
      wait for 20 ns;
      wait;
   end process;

END;
