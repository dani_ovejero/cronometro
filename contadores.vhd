----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:24:16 12/11/2015 
-- Design Name: 
-- Module Name:    contadores - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity contadores is
	 generic (width: positive:=4;
				 end_of_count : unsigned(3 downto 0) := "1001");
    Port (	clk 		: in 	STD_LOGIC;
				reset		: in 	STD_LOGIC;
				enable	: in  STD_LOGIC;
				load		: in 	STD_LOGIC;
				s_in		: in 	STD_LOGIC_VECTOR (width-1 downto 0);
				count		: out STD_LOGIC_VECTOR (width-1 downto 0);
				eoc		: out STD_LOGIC
			);
end contadores;

architecture Behavioral of contadores is
	signal cnt:unsigned(width-1 downto 0);
	signal sgn_eoc:std_logic;
begin
	process (clk,enable,reset) is
	begin
		if reset='1' then
			cnt <= "1001";
		elsif (clk'event and clk='1') then
			if (enable = '1') then
				if load='1' then 
					cnt<=unsigned(s_in);
				else
					if(cnt = "1001" - end_of_count) then
						cnt <= "1001";
						sgn_eoc <= '1';
					else
						cnt <= cnt - 1;
						sgn_eoc <= '0';
					end if;
				end if;
			end if;
		end if;
	end process;
	eoc <= sgn_eoc;
	count <= std_logic_vector(cnt);
end Behavioral;
