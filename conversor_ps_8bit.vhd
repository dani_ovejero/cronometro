----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:42:36 01/13/2016 
-- Design Name: 
-- Module Name:    conversor_ps - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity conversor_ps_8bit is
    Port ( input_0 : in  STD_LOGIC;
           input_1 : in  STD_LOGIC;
           input_2 : in  STD_LOGIC;
           input_3 : in  STD_LOGIC;
           input_4 : in  STD_LOGIC;
           input_5 : in  STD_LOGIC;
           input_6 : in  STD_LOGIC;
           serial_output : out  STD_LOGIC_VECTOR (6 downto 0));
end conversor_ps_8bit;

architecture Behavioral of conversor_ps_8bit is
	signal salida : std_logic_vector (6 downto 0);
begin
process (input_0,input_1,input_2,input_3,input_4,input_5,input_6)
	begin
		salida(0) <= input_0;
		salida(1) <= input_1;
		salida(2) <= input_2;
		salida(3) <= input_3;
		salida(4) <= input_4;
		salida(5) <= input_5;
		salida(6) <= input_6;
	end process;
serial_output <= salida;
end Behavioral;

