----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:42:13 01/15/2016 
-- Design Name: 
-- Module Name:    encoder_in - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity encoder_in is
    Port ( 
		clk 		: IN 	STD_LOGIC;
		reset 	: IN 	STD_LOGIC;
		A 			: IN  STD_LOGIC;
		B 			: IN  STD_LOGIC;
		e_up 		: OUT	STD_LOGIC;
		e_down 	: OUT	STD_LOGIC);
end encoder_in;

architecture Behavioral of encoder_in is

COMPONENT flnk_dtct
	
	PORT(
		mode		: IN  std_logic;
		input		: IN  std_logic;
		CLK		: IN  std_logic;
		RST		: IN  std_logic;          
		output	: OUT std_logic
		);
	END COMPONENT;


	signal evnt : STD_LOGIC;
	signal up 	: STD_LOGIC;
	signal down : STD_LOGIC;
	
	
begin

Inst_flnk_dtct: flnk_dtct PORT MAP(
		mode => '1',
		input => A,
		output => evnt,
		CLK => clk,
		RST => reset
	);
	
	process (clk, evnt)
	begin
		if(clk'event and clk = '1') then
			if (evnt = '1' and B = '1') then
				up		<= '1';
				down	<= '0';
			elsif(evnt = '1' and B = '0') then
				up		<= '0';
				down	<= '1';
			else
				up		<= '0';
				down	<= '0';
			end if;
		end if;
	end process;
	
	e_up <= up;
	e_down <= down;
	
end Behavioral;

