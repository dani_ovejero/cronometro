----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:26:02 01/08/2016 
-- Design Name: 
-- Module Name:    flnk_dtct - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flnk_dtct is
    Port (
				mode		: in	STD_LOGIC;
				input		: in	STD_LOGIC;
				output	: out	STD_LOGIC;
				CLK		: in	STD_LOGIC;
				RST		: in	STD_LOGIC);
end flnk_dtct;

architecture Behavioral of flnk_dtct is
	signal	last		: STD_LOGIC;
	signal	outstate	: STD_LOGIC;
begin
		process(CLK,RST)
		begin
			if (RST = '1') 
				then
					last <= '0';
					outstate <= '0';
			elsif (CLK'event and CLK = '1') 
				then
					last <= input;
					if (last /= input and mode = input) then outstate <= '1';
					else outstate <= '0';
					end if;
			end if;
		end process;
		
		output <= outstate;

end Behavioral;

