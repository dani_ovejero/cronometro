--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:26:12 01/21/2016
-- Design Name:   
-- Module Name:   C:/Users/USER1/Documents/cronometro/flnk_dtct_tb.vhd
-- Project Name:  cronometro_final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: flnk_dtct
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY flnk_dtct_tb IS
END flnk_dtct_tb;
 
ARCHITECTURE behavior OF flnk_dtct_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT flnk_dtct
    PORT(
         mode : IN  std_logic;
         input : IN  std_logic;
         output : OUT  std_logic;
         CLK : IN  std_logic;
         RST : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal mode : std_logic := '0';
   signal input : std_logic := '0';
   signal CLK : std_logic := '0';
   signal RST : std_logic := '1';

 	--Outputs
   signal output : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: flnk_dtct PORT MAP (
          mode => mode,
          input => input,
          output => output,
          CLK => CLK,
          RST => RST
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
			rst <= '0';
			mode <= '1';
		wait for 20 ns;
			input <= '1';
		wait for 10 ns;
			input <= '0';
		wait for 30 ns;
			input <= '1';
		wait for 20 ns;
			mode <= '0';
			input <= '0';
		wait for 20 ns;
			input <= '1';
		wait for 50 ns;
			mode <= '1';
			input <= '0';
		wait for 20 ns;
			input <= '1';
      wait for CLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
