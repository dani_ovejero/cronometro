--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:16:15 12/11/2015
-- Design Name:   
-- Module Name:   C:/Xilinx/14.7/ISE_DS/ISE/Escritorio/cronometro2/johnson_counter_tb.vhd
-- Project Name:  cronometro2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: jcnt
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY jcnt_tb IS
END jcnt_tb;
 
ARCHITECTURE behavior OF jcnt_tb IS 
 
-- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT jcnt
    PORT(
         CLK : IN  std_logic;
         RST : IN  std_logic;
         P_OUT : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    
--Inputs
   signal CLK : std_logic := '0';
   signal RST : std_logic := '0';

--Outputs
   signal P_OUT : std_logic_vector(3 downto 0);

-- Clock period definitions
   constant CLK_period : time := 20 ns;
 
BEGIN
 
-- Instantiate the Unit Under Test (UUT)
   uut: jcnt PORT MAP (
          CLK => CLK,
          RST => RST,
          P_OUT => P_OUT
        );

-- Clock process definitions
   CLK_process :process
   begin
  CLK <= '0';
  wait for CLK_period/2;
  CLK <= '1';
  wait for CLK_period/2;
   end process; 
 
-- Stimulus process
   stim_proc: process
   begin  
-- hold reset state for 100 ns.
   wait for 10 ns;
 RST <= '1';
 wait for 10 ns;
 RST <= '0';
 wait for 180 ns;

 ASSERT false
  REPORT "fin"
  SEVERITY FAILURE;

     end process;

END;