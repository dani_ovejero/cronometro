----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:31:53 01/14/2016 
-- Design Name: 
-- Module Name:    conversor_ps_4bit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity conversor_ps_4bit is
    Port ( input_0 : in  STD_LOGIC;
           input_1 : in  STD_LOGIC;
           input_2 : in  STD_LOGIC;
           input_3 : in  STD_LOGIC;
           serial_output : out  STD_LOGIC_VECTOR (3 downto 0));
end conversor_ps_4bit;

architecture Behavioral of conversor_ps_4bit is
	signal salida : std_logic_vector (3 downto 0);
begin
process (input_0,input_1,input_2,input_3)
	begin
		salida(0) <= input_0;
		salida(1) <= input_1;
		salida(2) <= input_2;
		salida(3) <= input_3;
	end process;
serial_output <= salida;
end Behavioral;
