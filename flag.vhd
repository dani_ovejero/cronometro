----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity flag is
port(
	activ : IN STD_LOGIC; 
	reset: IN STD_LOGIC;
	clk: IN STD_LOGIC;
	latch : OUT STD_LOGIC
	);
end flag;

architecture Behavioral of flag is
	signal status : STD_LOGIC;
begin
	process(clk)
	begin
		if(clk='1' and clk'event) then
			if reset = '1' then
				latch <= '0';
				status <= '0';
			elsif activ = '1' then
				latch <= '1';
				status <= '1';
			elsif status = '1' then
				latch <= '1';
			end if;
		end if;
		end process;
end Behavioral;

