--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   02:08:21 01/18/2016
-- Design Name:   
-- Module Name:   C:/Users/Guille/Documents/cronometro/top_tb.vhd
-- Project Name:  cronometro_final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY top_tb IS
END top_tb;
 
ARCHITECTURE behavior OF top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top
    PORT(
         clk : IN  std_logic;
         start_stop : IN  std_logic;
         hold_reset : IN  std_logic;
         leds : OUT  std_logic_vector(3 downto 0);
         segment : OUT  std_logic_vector(6 downto 0);
         anodes : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal start_stop : std_logic := '0';
   signal hold_reset : std_logic := '0';

 	--Outputs
   signal leds : std_logic_vector(3 downto 0);
   signal segment : std_logic_vector(6 downto 0);
   signal anodes : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top PORT MAP (
          clk => clk,
          start_stop => start_stop,
          hold_reset => hold_reset,
          leds => leds,
          segment => segment,
          anodes => anodes
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
	--Inicio - Reset
      hold_reset <= '1';
		
		wait for clk_period*15;
		hold_reset <= '0';
		
	--Inicio del crono
		wait for clk_period*5;
		start_stop <= '1';
		
		wait for clk_period*10;
		start_stop <= '0';
		
	--Guardado de valores			
		wait for clk_period*234;
		hold_reset <= '1';
		
		wait for clk_period*34;
		hold_reset <= '0';
		
		wait for clk_period*734;
		hold_reset <= '1';
		
		wait for clk_period*24;
		hold_reset <= '0';
		
		wait for clk_period*532;
		hold_reset <= '1';
		
		wait for clk_period*37;
		hold_reset <= '0';
		
	--Parada del crono
		wait for clk_period*583;
		start_stop <= '1';
		
		wait for clk_period*9;
		start_stop <= '0';
		
	--Muestra de valores
		wait for clk_period*234;
		hold_reset <= '1';
		
		wait for clk_period*34;
		hold_reset <= '0';
		
		wait for clk_period*734;
		hold_reset <= '1';
		
		wait for clk_period*24;
		hold_reset <= '0';
		
		wait for clk_period*532;
		hold_reset <= '1';
		
		wait for clk_period*37;
		hold_reset <= '0';
		
	--Reinicio del crono
		wait for clk_period*583;
		start_stop <= '1';
		
		wait for clk_period*9;
		start_stop <= '0';
	

      wait;
   end process;

END;
